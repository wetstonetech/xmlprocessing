/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package processing.core;

import java.lang.reflect.Array;

/**
 *
 * @author murchd
 */
public class PUtil {

    static public String[] split(String value, char delim) {
        // do this so that the exception occurs inside the user's
        // program, rather than appearing to be a bug inside split()
        if (value == null) {
            return null;
        }
        //return split(what, String.valueOf(delim));  // huh

        char chars[] = value.toCharArray();
        int splitCount = 0; //1;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == delim) {
                splitCount++;
            }
        }
        // make sure that there is something in the input string
        //if (chars.length > 0) {
        // if the last char is a delimeter, get rid of it..
        //if (chars[chars.length-1] == delim) splitCount--;
        // on second thought, i don't agree with this, will disable
        //}
        if (splitCount == 0) {
            String splits[] = new String[1];
            splits[0] = new String(value);
            return splits;
        }
        //int pieceCount = splitCount + 1;
        String splits[] = new String[splitCount + 1];
        int splitIndex = 0;
        int startIndex = 0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == delim) {
                splits[splitIndex++] =
                        new String(chars, startIndex, i - startIndex);
                startIndex = i + 1;
            }
        }
        //if (startIndex != chars.length) {
        splits[splitIndex] =
                new String(chars, startIndex, chars.length - startIndex);
        //}
        return splits;
    }

    static public boolean[] subset(boolean list[], int start) {
        return subset(list, start, list.length - start);
    }

    /**
     * ( begin auto-generated from subset.xml )
     *
     * Extracts an array of elements from an existing array. The <b>array</b>
     * parameter defines the array from which the elements will be copied and
     * the <b>offset</b> and <b>length</b> parameters determine which elements
     * to extract. If no <b>length</b> is given, elements will be extracted from
     * the <b>offset</b> to the end of the array. When specifying the
     * <b>offset</b> remember the first array element is 0. This function does
     * not change the source array. <br/> <br/> When using an array of objects,
     * the data returned from the function must be cast to the object array's
     * data type. For example: <em>SomeClass[] items = (SomeClass[])
     * subset(originalArray, 0, 4)</em>.
     *
     * ( end auto-generated )
     *
     * @webref data:array_functions
     * @param list array to extract from
     * @param start position to begin
     * @param count number of values to extract
     * @see PApplet#splice(boolean[], boolean, int)
     */
    static public boolean[] subset(boolean list[], int start, int count) {
        boolean output[] = new boolean[count];
        System.arraycopy(list, start, output, 0, count);
        return output;
    }

    static public byte[] subset(byte list[], int start) {
        return subset(list, start, list.length - start);
    }

    static public byte[] subset(byte list[], int start, int count) {
        byte output[] = new byte[count];
        System.arraycopy(list, start, output, 0, count);
        return output;
    }

    static public char[] subset(char list[], int start) {
        return subset(list, start, list.length - start);
    }

    static public char[] subset(char list[], int start, int count) {
        char output[] = new char[count];
        System.arraycopy(list, start, output, 0, count);
        return output;
    }

    static public int[] subset(int list[], int start) {
        return subset(list, start, list.length - start);
    }

    static public int[] subset(int list[], int start, int count) {
        int output[] = new int[count];
        System.arraycopy(list, start, output, 0, count);
        return output;
    }

    static public float[] subset(float list[], int start) {
        return subset(list, start, list.length - start);
    }

    static public float[] subset(float list[], int start, int count) {
        float output[] = new float[count];
        System.arraycopy(list, start, output, 0, count);
        return output;
    }

    static public String[] subset(String list[], int start) {
        return subset(list, start, list.length - start);
    }

    static public String[] subset(String list[], int start, int count) {
        String output[] = new String[count];
        System.arraycopy(list, start, output, 0, count);
        return output;
    }

    static public Object subset(Object list, int start) {
        int length = Array.getLength(list);
        return subset(list, start, length - start);
    }

    static public Object subset(Object list, int start, int count) {
        Class<?> type = list.getClass().getComponentType();
        Object outgoing = Array.newInstance(type, count);
        System.arraycopy(list, start, outgoing, 0, count);
        return outgoing;
    }

    /**
     * ( begin auto-generated from concat.xml )
     *
     * Concatenates two arrays. For example, concatenating the array { 1, 2, 3 }
     * and the array { 4, 5, 6 } yields { 1, 2, 3, 4, 5, 6 }. Both parameters
     * must be arrays of the same datatype. <br/> <br/> When using an array of
     * objects, the data returned from the function must be cast to the object
     * array's data type. For example: <em>SomeClass[] items = (SomeClass[])
     * concat(array1, array2)</em>.
     *
     * ( end auto-generated )
     *
     * @webref data:array_functions
     * @param a first array to concatenate
     * @param b second array to concatenate
     * @see PApplet#splice(boolean[], boolean, int)
     * @see PApplet#arrayCopy(Object, int, Object, int, int)
     */
    static public boolean[] concat(boolean a[], boolean b[]) {
        boolean c[] = new boolean[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    static public byte[] concat(byte a[], byte b[]) {
        byte c[] = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    static public char[] concat(char a[], char b[]) {
        char c[] = new char[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    static public int[] concat(int a[], int b[]) {
        int c[] = new int[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    static public float[] concat(float a[], float b[]) {
        float c[] = new float[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    static public String[] concat(String a[], String b[]) {
        String c[] = new String[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    static public Object concat(Object a, Object b) {
        Class<?> type = a.getClass().getComponentType();
        int alength = Array.getLength(a);
        int blength = Array.getLength(b);
        Object outgoing = Array.newInstance(type, alength + blength);
        System.arraycopy(a, 0, outgoing, 0, alength);
        System.arraycopy(b, 0, outgoing, alength, blength);
        return outgoing;
    }

    //
    /**
     * ( begin auto-generated from reverse.xml )
     *
     * Reverses the order of an array.
     *
     * ( end auto-generated )
     *
     * @webref data:array_functions
     * @param list booleans[], bytes[], chars[], ints[], floats[], or Strings[]
     * @see PApplet#sort(String[], int)
     */
    static public boolean[] reverse(boolean list[]) {
        boolean outgoing[] = new boolean[list.length];
        int length1 = list.length - 1;
        for (int i = 0; i < list.length; i++) {
            outgoing[i] = list[length1 - i];
        }
        return outgoing;
    }

    static public byte[] reverse(byte list[]) {
        byte outgoing[] = new byte[list.length];
        int length1 = list.length - 1;
        for (int i = 0; i < list.length; i++) {
            outgoing[i] = list[length1 - i];
        }
        return outgoing;
    }

    static public char[] reverse(char list[]) {
        char outgoing[] = new char[list.length];
        int length1 = list.length - 1;
        for (int i = 0; i < list.length; i++) {
            outgoing[i] = list[length1 - i];
        }
        return outgoing;
    }

    static public int[] reverse(int list[]) {
        int outgoing[] = new int[list.length];
        int length1 = list.length - 1;
        for (int i = 0; i < list.length; i++) {
            outgoing[i] = list[length1 - i];
        }
        return outgoing;
    }

    static public float[] reverse(float list[]) {
        float outgoing[] = new float[list.length];
        int length1 = list.length - 1;
        for (int i = 0; i < list.length; i++) {
            outgoing[i] = list[length1 - i];
        }
        return outgoing;
    }

    static public String[] reverse(String list[]) {
        String outgoing[] = new String[list.length];
        int length1 = list.length - 1;
        for (int i = 0; i < list.length; i++) {
            outgoing[i] = list[length1 - i];
        }
        return outgoing;
    }

    static public Object reverse(Object list) {
        Class<?> type = list.getClass().getComponentType();
        int length = Array.getLength(list);
        Object outgoing = Array.newInstance(type, length);
        for (int i = 0; i < length; i++) {
            Array.set(outgoing, i, Array.get(list, (length - 1) - i));
        }
        return outgoing;
    }

    //////////////////////////////////////////////////////////////
    // STRINGS
    /**
     * ( begin auto-generated from trim.xml )
     *
     * Removes whitespace characters from the beginning and end of a String. In
     * addition to standard whitespace characters such as space, carriage
     * return, and tab, this function also removes the Unicode "nbsp" character.
     *
     * ( end auto-generated )
     *
     * @webref data:string_functions
     * @param str any string
     * @see PApplet#split(String, String)
     * @see PApplet#join(String[], char)
     */
    static public String trim(String str) {
        return str.replace('\u00A0', ' ').trim();
    }

    /**
     * @param array a String array
     */
    static public String[] trim(String[] array) {
        String[] outgoing = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                outgoing[i] = array[i].replace('\u00A0', ' ').trim();
            }
        }
        return outgoing;
    }

    /**
     * ( begin auto-generated from join.xml )
     *
     * Combines an array of Strings into one String, each separated by the
     * character(s) used for the <b>separator</b> parameter. To join arrays of
     * ints or floats, it's necessary to first convert them to strings using
     * <b>nf()</b> or <b>nfs()</b>.
     *
     * ( end auto-generated )
     *
     * @webref data:string_functions
     * @param list array of Strings
     * @param separator char or String to be placed between each item
     * @see PApplet#split(String, String)
     * @see PApplet#trim(String)
     * @see PApplet#nf(float, int, int)
     * @see PApplet#nfs(float, int, int)
     */
    static public String join(String[] list, char separator) {
        return join(list, String.valueOf(separator));
    }

    static public String join(String[] list, String separator) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < list.length; i++) {
            if (i != 0) {
                buffer.append(separator);
            }
            buffer.append(list[i]);
        }
        return buffer.toString();
    }
}
